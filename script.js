var arraysOfColor = ["red","green","blue","brown","pink","yellow "];
let arraysOfCards = new Array();
var tableOfColorsForAllCards= new Array();
var chosenAmountOfCards;
var idDivdisplayedBoolean =false;
var idDivdisplayedId=-10;
var amounOfCardToMatch;


$("#startNewGame").click(defineAmountOfCard);
$("#cards").click(play);
$("#restartGame").click(restartGame);


function defineAmountOfCard(){
		if (($(AmountofCards).val())!=null){
  			chosenAmountOfCards=$(AmountofCards).val(); //read ampunt of cards from HTML form
    		$("#newGame").addClass("hidden" ); //hide menu for starting new game
    		$("#restartGame").removeClass( "hidden" ); //unhide menu for restrt game
    		initiateGame();
		};
};

function initiateGame(){
    CreateTableOfColorForGame(chosenAmountOfCards); //define color fo each of cards
		writeColorsToArraysOfCard(chosenAmountOfCards);//write color of every card to array arraysOfCards that represents all cards in game
		createCards(chosenAmountOfCards); //create html elements that represent the cards in game
};

function CreateTableOfColorForGame(amountsOfCards){
  	amountOfUniqeColors=amountsOfCards/2;
		newtabcolor=arraysOfColor.slice(0,amountOfUniqeColors); //create array with unique color for game by extract piece from array with unique color. Size of the piece is equal to amount of cards divide by two as every two cards have got the same color
    tableOfColorsForAllCards=newtabcolor.concat(newtabcolor); //tableOfColorsForAllCards - array that contains color for every card. So at the begining is created with array of array of unique color joined with itself
		tableOfColorsForAllCards.sort(function(){return 0.5-Math.random()}); //randomize order of elements in tableOfColorsForAllCards
};

function writeColorsToArraysOfCard(amountsOfCards){
//write color of every card to array arraysOfCards that represents all cards in game
//it's two dimensial array
//- in column [0] - color of card is written
//- in column [1]- value "0" indicates unpaired state of card, value "1" indicates that card is paired
	for (i=0;i<amountsOfCards;i++){
  		arraysOfCards[i] = new Array(1);
			arraysOfCards[i][0]=tableOfColorsForAllCards[i];
			arraysOfCards[i][1]=0;
	};
};

function createCards(amountsOfCards){
    //create html elements that represent the cards in game
    //html elements are displayed in two rows
		for (i=0;i<amountsOfCards/2;i++){
				$("#main1").append('<div id="'+i+'" class="border white"><br></div>');
		};
		for (i=amountsOfCards/2;i<arraysOfCards.length;i++){
				$("#main2").append('<div id="'+i+'" class="border white"><br></div>');
		};
    amounOfCardToMatch=amountsOfCards;
};

function play(event){
    //play the game
	if(ifAlreadyWon(event.target.id)){//user clicked on cards that had been already paired with other card with the same color - nothing happen
    return;
  }
  else {
  		if(!ifAnyDivDisplayed()){ //unhide the card that was hidden - first click on the card
      	  	show(event,checkColor(event.target.id)); //unhide card
      	  	divDispleydYes(event.target.id);
    	}
    	else{
    			if(ifDivClickedIsDivDisplayed(event.target.id)){//hide again card that was unhiden - second click on the same card
      					hide(event,checkColor(event.target.id)); //hide the card
         	   		divDispleydNO();
      		}
    			else{
 							if(ifDivClickedHasColorDivDisplayed(event.target.id)){
          				//alert("win")
                        //user click and unhide card that has got the same color as card that is being already unhided as last one.
                        //both cards that are being paired will remain unhiden till the end of game
                		show(event,checkColor(event.target.id));
                		writeDivSolved(event.target.id);
                		writeDivSolved(idDivdisplayedId);
                		divDispleydNO();
                  	amounOfCardToMatch=amounOfCardToMatch-2;
                  	CheckIfEndOfGame(); //check if other cards are still unpaired
          		}
       				else{
                        //user click and unhide card that has got different color that card that is being already already unhided as last one.
                        //So, both cards will be again hidden after 500 ms.
										show(event,checkColor(event.target.id));
               			setTimeout(() => {
                		  	hide(event,checkColor(event.target.id));
                  			hideId(idDivdisplayedId,checkColor(idDivdisplayedId));
                  			divDispleydNO();
              	 		}, 500)
            	}
        }
    }
  }
}

function CheckIfEndOfGame(){
	if(amounOfCardToMatch==0){
  		alert("Congratulation, you successfully ended the game!");
  };
};

function ifAlreadyWon(id){
    //check if card is already paired with other card
	return arraysOfCards[id][1];
};

function ifAnyDivDisplayed(){
    //check if any other unpaired card is being unhidden.
	return idDivdisplayedBoolean;
};

function ifDivClickedIsDivDisplayed(id){
    //check if user click on unpaired card that is being unhidden
	return id==idDivdisplayedId;
};

function ifDivClickedHasColorDivDisplayed(id){
    //check if catrd that wac clicked has got the same color as unpaired card that is being unhidden.
	return arraysOfCards[id][0]==arraysOfCards[idDivdisplayedId][0];
};

function checkColor(id){
    //check color of card
	return arraysOfCards[id][0];
};

function writeDivSolved(id){
    //write that card was paired
	arraysOfCards[id][1]=1;
};

function divDispleydYes(id){
    //write that some unpaired card is being unhidden
    idDivdisplayedBoolean=true;
    idDivdisplayedId=id;
};

function divDispleydNO(){
    //write that any unpaired card is being unhidden
    idDivdisplayedBoolean=false;
    idDivdisplayedId=-10;
};

function show(event,color) {
    //unhide card
	  $(event.target).removeClass( "white" ).addClass( color );
};

function hide(event,color) {
    //hide card that was clicked
	  $(event.target).removeClass( color ).addClass( "white" );
};   
 
function hideId(id,color) {
    //hide card that has got selected id
 		tmp="#"+id;
 	  $(tmp).removeClass( color ).addClass( "white" );
};

function restartGame(){
		for (i=chosenAmountOfCards;i>-1;i--){
					arraysOfCards.pop();
    	    tableOfColorsForAllCards.pop();
		};
  	divDispleydNO();
  	$("#main1").empty();
  	$("#main2").empty();
    $("#newGame").removeClass( "hidden" );
    $("#restartGame").addClass("hidden" );
};